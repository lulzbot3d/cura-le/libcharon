# LulzBot fork of Ultimaker's libCharon used for Cura LE

## libCharon
File metadata and streaming library


## Important Details
To install this library in a python install that was built by cura-le-build-environment, the following commands should be run from the root directory of this repository:  
`mkdir build && cd build`. 

`cmake -DPython3_EXECUTABLE=<Path to your environment directory>/bin/python3 -DCMAKE_INSTALL_PREFIX=<Path to your environment directory>/ ..`
